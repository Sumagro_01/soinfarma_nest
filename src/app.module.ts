import { Module } from '@nestjs/common';
import { EasyconfigModule } from 'nestjs-easyconfig';
import { ConfigurationModule } from './configuration/config.module';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DatabaseConfigurations } from './utils/database/database';
import { AuthModule } from './auth/auth.module';
import { ClientModule } from './client/client.module';
import { VisitorModule } from './visitor/visitor.module';
import { ExcelModule } from './excel/excel.module';
require('dotenv').config();

@Module({
  imports: [
    EasyconfigModule.register({ path: `environment/.env.${process.env.NODE_ENV}`, safe: true }),
    TypeOrmModule.forRootAsync(DatabaseConfigurations),
    ConfigurationModule,
    AuthModule,
    ClientModule,
    VisitorModule,
    ExcelModule,
  ],
})
export class AppModule {}
