import { Body, Controller, Get, Post, UseGuards } from '@nestjs/common';
import { UserEntity } from './../user/entities/user.entity';
import { AuthService } from './auth.service';
import { hasRoles } from './decorators/role.decorator';
import { GetUser } from './decorators/user.decorator';
import { LoginLawyerGoogleDto } from './dto/loginLawyerGoogle.dto';
import { LoginUserDto } from './dto/loginUser.dto';
import { RegisterLawyerDto } from './dto/registerLawyer.dto';
import { Roles } from './enum/roles';
import { JwtAuthGuard } from './guards/jwtAuth.guard';
import { RolesGuard } from './guards/role.guard';

@Controller('auth')
export class AuthController {
    constructor(
        private readonly authService: AuthService
    ) { }

    /* LOGIN */
    @Post('login')
    async login(@Body() loginUserDto: LoginUserDto ): Promise<any>{
        return await this.authService.login(loginUserDto);
    }


    /* REFRESH TOKEN DEVELOPER */
    @Get('profile')
    @UseGuards(RolesGuard)
    @UseGuards(JwtAuthGuard)
    @hasRoles(Roles.Lawyer)
    async profile(
        @GetUser() user: number,
    ): Promise<any>{
        return await this.authService.validateUser(user);
    }
    

}
