import { Injectable, NotFoundException, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt'
import { UserEntity } from './../user/entities/user.entity';
import { RegisterLawyerDto } from './dto/registerLawyer.dto';
import { UserService } from 'src/user/user.service';
import { RoleService } from 'src/role/role.service';
import { Roles } from './enum/roles';
import { LoginLawyerGoogleDto } from './dto/loginLawyerGoogle.dto';
import { LoginUserDto } from './dto/loginUser.dto';
@Injectable()
export class AuthService {
    constructor(
        private readonly jwtService: JwtService,
        private readonly userService: UserService,
        private readonly roleService: RoleService,
    ) { }

    async register(registerLawyerDto: RegisterLawyerDto): Promise<any>{
        const user = await this.userService.store(registerLawyerDto)
        await this.roleService.store(user, Roles.Lawyer);
        return await this.validateUser(user.id).then((userData) => {
            const token = this.createToken(userData);
            return {
                statusCode: 200,
                access_token: token,
            }
        }); 
    }

    /* LOGIN */
    async login(loginUserDto: LoginUserDto): Promise<any>{
        const user = await this.userService.login(loginUserDto);
        if (!user) {
            throw new UnauthorizedException('Usuario no registrado')
        }
        const token = this.createToken(user);
        return {
            statusCode: 200,
            access_token: token,
            user: user,
        }
    }

    async validateUser(id: number): Promise<UserEntity>{
        const user = await this.userService.get(id);
        if (!user) {
            return null;
        }
        return user;
    }

    /* GET USER */
    async profile(id: number): Promise<UserEntity>{
        return await this.validateUser(id);
    }

    /* CHECK ACCOUNT VALIDATED */
    async check_account(user_id: number): Promise<Boolean>{
        return await this.userService.check_account(user_id);
    }

    
    /* CHECK ACCOUNT VALIDATED */
    async check_email(user_id: number): Promise<Boolean>{
        return await this.userService.check_email(user_id);
    }

    /* REFRESH TOKEN */
    async refresh(token: string): Promise<any>{
        const payload = this.jwtService.verify(token);
        if (!payload) {
            throw new UnauthorizedException;
        }
       return await this.validateUser(payload.id).then((userData) => {
            const Token = this.createToken(userData);
            return {
                userData,
                access_token: Token,
                statusCode: 200
            }
        }); 
    }

    /* SIGN IN WITH GOOGLE */
    async loginWithGoogle(loginLawyerGoogleDto: LoginLawyerGoogleDto): Promise<any>{
        const { email } =  loginLawyerGoogleDto;
        const user = await this.userService.findByEmail(email);
        if(!user){
            let resp_user = await this.userService.loginWithGoogle(loginLawyerGoogleDto);
            await this.roleService.store(resp_user, Roles.Lawyer);
            return await this.validateUser(resp_user.id).then((userData) => {
                const token = this.createToken(userData);
                return {
                    statusCode: 200,
                    access_token: token,
                }
            }); 
        }
        return await this.validateUser(user.id).then((userData) => {
            const token = this.createToken(userData);
            return {
                statusCode: 200,
                access_token: token,
            }
        }); 
    }

     /* REFRESH TOKEN */
    async logout(token: string): Promise<any>{
        const payload = this.jwtService.verify(token);
        if (!payload) {
            throw new UnauthorizedException;
        }
       return await this.validateUser(payload.id).then((userData) => {
            const Token = this.createToken(userData);
            return {
                userData,
                access_token: Token,
                statusCode: 200
            }
        }); 
    }

  
    createToken(userData: any) {
        const payload = {
            id: userData.id,
            email: userData.email,
            password: userData.password,
        };
        return this.jwtService.sign(payload);
    }


}
