
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class LoginLawyerGoogleDto {

    @IsString()
    @IsNotEmpty()
    email: string;

    @IsString()
    @IsOptional()
    name: string;

    @IsString()
    @IsOptional()
    photoUrl: string;
}
