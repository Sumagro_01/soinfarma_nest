
import { IsNotEmpty, IsOptional, IsString } from 'class-validator';

export class RegisterLawyerDto {

    @IsString()
    @IsNotEmpty({ message: 'El correo es requerido' })
    email: string;

    @IsString()
    @IsNotEmpty({ message: 'La contraseña es requerida' })
    password: string;

    @IsString()
    @IsNotEmpty()
    name: string;

    @IsString()
    @IsOptional()
    provide: string;

    @IsString()
    @IsOptional()
    photoUrl: string;


}

