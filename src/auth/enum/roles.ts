export enum Roles {
    Admin = 'Admin',
    Lawyer = 'Lawyer',
    Client = 'Client',
}