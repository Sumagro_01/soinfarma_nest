import { Controller, Post, Body, HttpStatus, Get } from '@nestjs/common';
import { ClientService } from './client.service';
import { ContactDto } from './dto/contact.dto';
import { CreateClientDto } from './dto/create-client.dto';
import { ClientEntity } from './entities/client.entity';

@Controller('client')
export class ClientController {
  constructor(private readonly clientService: ClientService) {}

  @Post()
  async create(
    @Body() CreateClientDto: CreateClientDto
    ): Promise<HttpStatus.ACCEPTED> {
    return await this.clientService.create(CreateClientDto);
  }

  @Post('contact')
  async contact(
    @Body() contactDto: ContactDto
    ): Promise<HttpStatus.ACCEPTED> {
    return await this.clientService.contact(contactDto);
  }

  @Get()
  async get(): Promise<ClientEntity[]>{
    return await this.clientService.all();
  }

  @Get('length')
  async getLenght(): Promise<number>{
    return await this.clientService.length();
  }
}
