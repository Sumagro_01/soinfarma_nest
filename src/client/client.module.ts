import { Module } from '@nestjs/common';
import { ClientService } from './client.service';
import { ClientController } from './client.controller';
import { ClientRepository } from './repository/client.repository';
import { TypeOrmModule } from '@nestjs/typeorm';
import { MailModule } from 'src/mail/mail.module';

@Module({
  imports:[
    MailModule,
    TypeOrmModule.forFeature([ClientRepository])
  ],
  controllers: [ClientController],
  providers: [ClientService],
  exports:[ ClientService],
})
export class ClientModule {}
