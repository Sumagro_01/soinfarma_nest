import { HttpStatus, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { MailService } from 'src/mail/mail.service';
import { ContactDto } from './dto/contact.dto';
import { CreateClientDto } from './dto/create-client.dto';
import { UpdateClientDto } from './dto/update-client.dto';
import { ClientEntity } from './entities/client.entity';
import { ClientRepository } from './repository/client.repository';

@Injectable()
export class ClientService {

  constructor(
    @InjectRepository(ClientRepository)
    private readonly clientRepository: ClientRepository,
    private readonly mailService: MailService,
  ){}

  async create(createClientDto: CreateClientDto): Promise<HttpStatus.ACCEPTED> {
    return await this.clientRepository.store(createClientDto);
  }

  async contact(contactDto: ContactDto): Promise<HttpStatus.ACCEPTED> {
    this.mailService.contact(contactDto);
    return HttpStatus.ACCEPTED;
  }

  async all(): Promise<ClientEntity[]>{
    return await this.clientRepository.find({
      where: {
        deleted_at: null
      }
    });
  }

  async length(): Promise<number>{
    return await this.clientRepository.count({
      where: {
        deleted_at: null
      }
    })
  };
  
}
