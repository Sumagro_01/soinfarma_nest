import { IsEmail, IsNotEmpty, IsString } from "class-validator";

export class ContactDto {

    @IsString()
    name: string;c

    @IsEmail()
    email: string;

    @IsString()
    phone: string;

    @IsString()
    business: string;

    @IsString()
    message: string;
    
}
