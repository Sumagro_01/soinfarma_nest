import { IsEmail, IsNotEmpty, IsString } from "class-validator";

export class CreateClientDto {

    @IsString()
    name: string;c

    @IsEmail()
    email: string;

    @IsString()
    business: string;

    @IsString()
    state: string;

    @IsString()
    phone: string;
    
}
