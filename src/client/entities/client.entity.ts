import { Column, CreateDateColumn, Entity, OneToMany, OneToOne, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity('client')
export class ClientEntity{
    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'name' , type: 'varchar', length: '255', nullable: false})
    name: string;

    @Column({ name: 'email' , type: 'varchar', length: '255', nullable: false})
    email: string;

    @Column({ name: 'business' , type: 'varchar', length: '255', nullable: false})
    business: string;

    @Column({ name: 'state' , type: 'varchar', length: '255', nullable: false})
    state: string;

    @Column({ name: 'phone' , type: 'varchar', length: '255'})
    phone: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;


}