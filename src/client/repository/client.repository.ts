import { HttpStatus } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { CreateClientDto } from "../dto/create-client.dto";
import { ClientEntity } from "../entities/client.entity";

@EntityRepository(ClientEntity)
export class ClientRepository extends Repository<ClientEntity>{

    /* STORE */
    async store(createClientDto: CreateClientDto): Promise<HttpStatus.ACCEPTED>{
        const user = this.create(createClientDto)
        const resp = await this.save(user);
        return HttpStatus.ACCEPTED;
    }

}