import { Controller, Res, Header, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { ExcelService } from './excel.service';
import { Response } from 'express';
import { CreateExcelDto } from './dto/create-excel.dto';
import { UpdateExcelDto } from './dto/update-excel.dto';

@Controller('excel')
export class ExcelController {
  constructor(private readonly excelService: ExcelService) {}


  @Get('client/donwload')
  @Header('content-type', 'text/xlsx')
  async downloadClientExcel(
    @Res() res: Response
  ){
    let result = await this.excelService.downloadClientExcel();
    res.download(`${result}`);
  }
}
