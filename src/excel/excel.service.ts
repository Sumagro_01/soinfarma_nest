import { Injectable, NotFoundException, BadRequestException } from '@nestjs/common';
import { doc } from 'prettier';
import { Workbook } from 'exceljs';
import * as tmp from 'tmp';
import { ClientService } from 'src/client/client.service';
import { ClientEntity } from 'src/client/entities/client.entity';

@Injectable()
export class ExcelService {

  constructor(
    private readonly _clientService: ClientService,
  ){}

  async downloadClientExcel(): Promise<any>{
   
    let titleRow = ['id','nombre', 'email', 'telefono', 'negocio', 'estado', 'fecha de registro'];

    // createing a workbook
    let book = new Workbook

    // adding a worksheet to workbook 
    let sheet = book.addWorksheet(`sheet1`);

    const clientes = await this._clientService.all();

    if(!clientes){
      throw new NotFoundException("No data to download")
    }

    titleRow.unshift()

    sheet.addRow(titleRow);

    clientes.forEach((cliente:ClientEntity) => {
      sheet.addRow([
        cliente.id,
        cliente.name,
        cliente.email,
        cliente.phone,
        cliente.business,
        cliente.state,
        cliente.created_at
      ]);
    })

    this.stylesheet(sheet);

    let File = await new Promise((resolve, reject) => {
      tmp.file({
        discardDescriptor: true,
        prefix: `Lista_de_clientes`,
        postfix: '.xlsx',
        mode: parseInt('0600', 8)
      }, async (err, file) => {
          if(err){
            throw new BadRequestException(err);
          }

          book.xlsx.writeFile(file).then(_ => {
            resolve(file);
          }).catch(err => {
            throw new BadRequestException(err)
          })
      })
    })
    return File;
  }

  private stylesheet(sheet){
    sheet.getColumn(1).width = 20.5;
    sheet.getColumn(2).width = 20.5;
    sheet.getColumn(3).width = 20.5;
    sheet.getColumn(4).width = 20.5;
    sheet.getColumn(5).width = 20.5;

    sheet.getRow(1).height = 30.5

    sheet.getRow(1).font = { size: 11.5, bold: true, color: { argb: '000000'}}

    sheet.getRow(1).fill = { type: 'pattern', pattern: 'solid', bgColor: { argb: '56B746'}, fgColor: { argb: '56B746'}}

    sheet.getRow(1).aligment = { vertical: "middle", horizontal: "center", warpText: true}

    sheet.getRow(1).border = {
      top: { style: 'thin', color: { argb: '56B746'}},
      left: { style: 'thin', color: { argb: 'FFFFFF'}},
      bottom: { style: 'thin', color: { argb: '56B746'}},
      right: { style: 'thin', color: { argb: 'FFFFFF'}},
      
    }
  
  }
}
