import { MailerService } from '@nestjs-modules/mailer';
import { BadRequestException, HttpStatus, Injectable } from '@nestjs/common';
import { ContactDto } from 'src/client/dto/contact.dto';
import { ClientEntity } from 'src/client/entities/client.entity';
import { UserEntity } from 'src/user/entities/user.entity';

@Injectable()
export class MailService {
  constructor(private mailerService: MailerService) {}

    contact(contactDto: ContactDto) {
        try {
            const mail = this.mailerService.sendMail({
                to: 'soinfarma@farservice.net',
                subject: 'Contacto de landing Soinfarma',
                template: './contact',
                context: {
                    contactDto
                }
            });
            if (!mail) {
                throw new BadRequestException('Email not send');
            }
            return {
                statusCode: HttpStatus.ACCEPTED
            }
        } catch {
            return { 
                statusCode: HttpStatus.ACCEPTED,
            }
        }
  }
}