import { BadRequestException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { LoginLawyerGoogleDto } from 'src/auth/dto/loginLawyerGoogle.dto';
import { LoginUserDto } from 'src/auth/dto/loginUser.dto';
import { RegisterLawyerDto } from 'src/auth/dto/registerLawyer.dto';
import { MailService } from 'src/mail/mail.service';
import { UserEntity } from './entities/user.entity';
import { UserRepository } from './repository/user.repository';

@Injectable()
export class UserService {
    constructor(
        @InjectRepository(UserRepository)
        private readonly _userRepository: UserRepository,
        private readonly _mailService:  MailService,
    ) { }
    
    /* STORE */
    async store(registerLawyerDto: RegisterLawyerDto): Promise<any>{
        const { email } = registerLawyerDto;
        const resp_email = await this.findByEmail(email);
        if(resp_email){
            throw new BadRequestException('El correo electronico ya existe');
        }
        const user = await this._userRepository.store(registerLawyerDto);
        return user;
    }

    async index(): Promise<any>{
        return await this._userRepository.find();
    }

    /* LOGIN USER */
    async login(loginUserDto: LoginUserDto): Promise<UserEntity>{
        const { email, password } = loginUserDto;
        return await this._userRepository.findOne({
            where: { email: email, password: password }
        });
    }

    /* LOGIN WITH GOOGLE */
    async loginWithGoogle(loginLawyerGoogleDto: LoginLawyerGoogleDto): Promise<any>{
        const { email } = loginLawyerGoogleDto;
        const resp_email = await this.findByEmail(email);
        if(!resp_email){
            return await this._userRepository.storeWithGoogle(loginLawyerGoogleDto)
        }
        return null;
    }

    /* GET */
    async get(user_id: number): Promise<UserEntity>{
        return await this._userRepository.findOne({
            where: { id: user_id },
            relations: ['roles', 'lawyer_id'],
        });
    }
    
    /* CHECK ACCOUNT USER */
    async check_account(user_id:number): Promise<boolean>{
        const account =  await this._userRepository.createQueryBuilder('user')
                    .where('user.id = :user_id',{
                        user_id
                    })
                    .andWhere('user.account_verefied_at IS NOT NULL')
                    .getOne();
        if(account){
            return true;
        }
        return false;
    }

     /* CHECK EMAIL USER */
     async check_email(user_id:number): Promise<boolean>{
        const account =  await this._userRepository.createQueryBuilder('user')
                    .where('user.id = :user_id',{
                        user_id
                    })
                    .andWhere('user.email_verefied_at IS NOT NULL')
                    .getOne();
        if(account){
            return true;
        }
        return false;
    }
    
    /* FIND USER BY EMAIL */
    async findByEmail(email: string): Promise<UserEntity> {
        const user = await this._userRepository.findOne({
            where: { email: email },
        });
        if (user) {
           return user;
        }
        return null;
    }
}
