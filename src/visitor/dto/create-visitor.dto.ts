import { IsString } from "class-validator";

export class CreateVisitorDto {
    @IsString()
    ip: string;
}
