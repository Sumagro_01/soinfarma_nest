import { Column, CreateDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity('visitor')
export class VisitorEntity {

    @PrimaryGeneratedColumn()
    id: number;

    @Column({ name: 'ip' , type: 'varchar', length: '255', nullable: false})
    ip: string;

    @CreateDateColumn()
    created_at: Date;

    @UpdateDateColumn()
    updated_at: Date;

    @Column({ type: 'timestamp', nullable: true })
    deleted_at: Date;
}
