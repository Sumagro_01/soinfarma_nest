import { LoginLawyerGoogleDto } from "src/auth/dto/loginLawyerGoogle.dto";
import { RegisterLawyerDto } from "src/auth/dto/registerLawyer.dto";
import { EntityRepository, Repository } from "typeorm";
import { CreateVisitorDto } from "../dto/create-visitor.dto";
import { VisitorEntity } from "../entities/visitor.entity";

@EntityRepository(VisitorEntity)
export class VisitorRepository extends Repository<VisitorEntity>{

    /* STORE */
    async store(createVisitorDto: CreateVisitorDto): Promise<any>{
        const { ip } = createVisitorDto;
        const visitor = this.create({
            ip: ip
        })
        const resp = await this.save(visitor);
        return resp;
    }

}