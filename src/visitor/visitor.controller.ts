import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { VisitorService } from './visitor.service';
import { CreateVisitorDto } from './dto/create-visitor.dto';

@Controller('visitor')
export class VisitorController {
  constructor(private readonly visitorService: VisitorService) {}

  @Post()
  async create(@Body() createVisitorDto: CreateVisitorDto) {
    return await this.visitorService.create(createVisitorDto);
  }

  @Get()
  async length(): Promise<number> {
    return await this.visitorService.length();
  }
}
