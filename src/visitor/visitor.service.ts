import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { CreateVisitorDto } from './dto/create-visitor.dto';
import { UpdateVisitorDto } from './dto/update-visitor.dto';
import { VisitorRepository } from './repository/visitor.repository';

@Injectable()
export class VisitorService {

  constructor(
      @InjectRepository(VisitorRepository)
      private readonly visitorRepository: VisitorRepository
  ){}

  async create(createVisitorDto: CreateVisitorDto) {
     return await this.visitorRepository.store(createVisitorDto);
  }

  async length(): Promise<number>{
    return await this.visitorRepository.count({ 
        where: {
          deleted_at: null
        }
    });
  }

}
